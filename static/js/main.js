// automatically add links to images to itself (fullsize)
// to all images of class "photo" (found in "galleries" of posts)
$('.photo img').each(function(ix, image){
    $(image).parent().wrap('<a target="_blank" href="' + $(image).attr('src') + '"></a>');
  })